# MyFantasy
## Pour compiler et lancer le programme
```
cd src
javac *.java
java Main
```

## Scénario de test pour tester le fonctionnement
Se déplacer sur la première parcelle voisine et voir que le gnome 'Romain' qui était dans la parcelle passe dans l'état vulnérable.
Solliciter les gnomes une fois dans cette parcelle. Apercevoir que **Ceryan** qui était dans l'état isolé passe dans l'état protégé grâce à vous et rejoint votre tribu.
Tandis que **Deanalyan** qui lui, est dans l'état vulnérable, aura une chance sur deux de vous rejoindre.
Ensuite, vous pouvez vérifiez votre tribu avec le menu **Mes informations** et le sous-menu **Afficher ma tribu** (Les gnomes ce sont bien ajoutés à votre tribu)

## Ce qui fonctionne
* L'interface utilisateur (Menu des actions)
* L'implémentation du monde (Elfes, Gnomes, Parcelle et tribu)
* Le déplacement du joueur sur une parcelle voisine
* Lorsque le joueur se déplace sur une autre parcelle, les gnomes de sa tribu deviennent vulnérable
* Le joueur peut solliciter les gnomes vulnérable ou isolé de sa parcelle actuelle

## Ce qui ne fonctionne pas encore
* L'implémentation de l'intelligence artificielle (Pour un elfe non-joueur)
* Réactions des gnomes autres que lorsque le chef part


## Développeur
* **Romain HERAULT** - *Développeur* - [r.herault](http://rherault.fr)
