# Gnome
- [x] FAIT
# Elfes
- [ ] FAIT

## Tous 
### Se déplacer
L'elfe se déplacera sur une parcelle voisine.
```java
public void seDeplacer(Parcelle parcelle){ }
```

> Système de parcelle voisine ?
> Choix de la parcelle ou random entre les parcelles voisines ?

----

### Répondre à une sollicitation
L'elfe répondra a une sollicitation d'un chef de tribu extérieure en choisissant, ou non, de le rejoindre 
```java
public void repondreSollicitation(Elfe elfeChef){ }
```
> Random ? Choix utilisateur ? IJoueur ➡️ IA ou humain
> Si IA ➡️ random sinon choix

----

### Former une tribu
Dans le cas où il est le seul elfe de sa parcelle ; tous les gnomes de sa tribu présents sur la-dite parcelle se rallieront à ce nouveau chef. Sa nouvelle tribu deviendra une sous-tribu de l'ancienne
```java
public void formerTribu(){ }
```
> Événement automatique ? Choix utilisateur ? 
> Pour tout les gnomes de la parcelle ➡️ se rallier à cet elfe

## Chef uniquement
> IStatut ➡️ chef ou normal

### Solliciter
Les gnomes des autres tribus présents sur sa parcelle et ses parcelles voisines seront solliciter, afin d'agrandir sa tribu. 
⚠️ Il ne pourra rallier dans sa tribu que les gnomes des tribus non dominées par la sienne ou non dominante de la sienne.
```java
public void solliciter(){ }
```
> Pour tout les gnomes de la parcelle et des parcelles voisines ➡️ `elfe.solliciter(this)`

----

### S'émanciper
S'emanciper de sa hiérarchie en "coupant" son lien de domination avec la tribu dont il est issu. 
⚠️ Cette action est irréversible et fragilise sa tribu (et ses sous-tribu) puisque les gnomes risquent d'être d'avantage solliciter par la suite.
```java
public void emanciper(){ }
```
> Ne plus être dominé par une tribu au-dessus
> Création d'une tribu sans tribu au dessus
> Tribu indépendante

----

### Négocier
Négocier avec un chef d'une tribu non dominée par la sienne et non dominante de la sienne à condition que les deux chefs soient sur des parcelles voisines.
Le résultat d'une négociation entre tribus peut conduire :
soit au statuquo (les deux tribus demeurent avec éventuellement des échanges de gnomes d'une tribu vers l'autre)
soit à la fusion des deux tribus dont l'un des deux chefs (le gagnant de la négociation) prendra la tête tandis que l'autre deviendra simple Elfe de cette grande tribu.
```java
public void negocier(){ }
```
> Résultat de la négociation aléatoire
> Si 1 ➡️ statuquo et random d'échange de bizu
> Si 2 ➡️ fusion des 2 tribus et gagnant de la négociation devient chef
> Système de gagnant/perdant