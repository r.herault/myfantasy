import java.util.*;


/**
 * Class Elfe
 */
public class Elfe extends Personnage{

    //
    // Fields
    //

    private IStatut statut;

    //
    // Constructors
    //
    public Elfe(String nom) {
        super(nom);
        this.statut = new ElfeNormal();
    }

    //
    // Methods
    //

    public void setStatut(IStatut statut){
        this.statut = statut;
    }

    public IStatut getStatut(){
        return this.statut;
    }

    /**
     * L'elfe se déplacera sur une parcelle voisine.
     */
    public void seDeplacer(Parcelle parcelle){
        if(this.statut instanceof ElfeNormal){
            this.changeParcelleActuelle(parcelle);
        }else if(this.statut instanceof ElfeChef){
            ArrayList<Gnome> listeGnome = new ArrayList<>();
            for(Personnage perso : this.getParcelleActuelle().getListePersonnageParcelle()){
                if(this.getTribu().equals(perso.getTribu()) && perso instanceof Gnome){
                    Gnome gnome = (Gnome) perso;
                    listeGnome.add(gnome);
                }
            }
            for(Gnome gnome : listeGnome){
                gnome.chefPart();
            }
            this.changeParcelleActuelle(parcelle);
        }
    }

    /**
     * L'elfe répondra a une sollicitation d'un chef de tribu extérieure en choisissant, ou non, de le rejoindre
     */
    public void repondreSollicitation(){
        
    }

    /**
     * Dans le cas où il est le seul elfe de sa parcelle ; tous les gnomes de sa tribu présents sur la-dite parcelle se rallieront à ce nouveau chef. Sa nouvelle tribu deviendra une sous-tribu de l'ancienne
     */
    public void formerTribu(){
        
    }

    /**
     * [LORSQU'IL EST CHEF] Les gnomes des autres tribus présents sur sa parcelle et ses parcelles voisines seront solliciter, afin d'agrandir sa tribu.
     * /!\ Il ne pourra rallier dans sa tribu que les gnomes des tribus non dominées par la sienne ou non dominante de la sienne.
     */
    public void solliciter(Elfe elfe){
        this.statut.solliciter(elfe);
    }

    /**
     * [LORSQU'IL EST CHEF] S'emanciper de sa hiérarchie en "coupant" son lien de domination avec la tribu dont il est issu.
     * /!\ Cette action est irréversible et fragilise sa tribu (et ses sous-tribu) puisque les gnomes risquent d'être d'avantage solliciter par la suite.
     */
    public void emanciper(){
        this.statut.emanciper();
    }

    /**
     * [LORSQU'IL EST CHEF] Négocier avec un chef d'une tribu non dominée par la sienne et non dominante de la sienne à condition que les deux chefs soient sur des parcelles voisines.
     * Le résultat d'une négociation entre tribus peut conduire :
     * soit au statuquo (les deux tribus demeurent avec éventuellement des échanges de gnomes d'une tribu vers l'autre)
     * soit à la fusion des deux tribus dont l'un des deux chefs (le gagnant de la négociation) prendra la tête tandis que l'autre deviendra simple Elfe de cette grande tribu.
     */
    public void negocier(){
        this.statut.negocier();
    }

    public String toString(){
        if(this.statut instanceof ElfeChef){
            return "\u001B[34m" + "[CHEF] " + this.nom + "\u001B[0m";
        }
        return "\u001B[34m" + this.nom + "\u001B[0m";
    }
}
