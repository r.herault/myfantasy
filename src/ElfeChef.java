import java.util.*;

/**
 * Class ElfeChef
 */
public class ElfeChef implements IStatut {

    //
    // Fields
    //


    //
    // Constructors
    //
    public ElfeChef() {}

    public void solliciter(Elfe elfe){
        ArrayList<Gnome> listeGnome = new ArrayList<>();
        for(Personnage perso : elfe.getParcelleActuelle().getListePersonnageParcelle()){
            if(perso instanceof Gnome){
                Gnome gnome = (Gnome) perso;
                listeGnome.add(gnome);
            }
        }

        for(Gnome gnome : listeGnome){
            gnome.solliciter(elfe);
        }
    }

    public void emanciper(){
        
    }

    public void negocier(){
        
    }
}

