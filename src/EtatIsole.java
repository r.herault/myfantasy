/**
 * Class Isole
 */
public class EtatIsole implements IEtat {

    //
    // Fields
    //


    //
    // Constructors
    //
    public EtatIsole () { };

    //
    // Methods
    //


    //
    // Accessor methods
    //

    //
    // Other methods
    //

    /**
     * @param        gnome
     */
    public void elfesPartent(Gnome gnome){
    }


    /**
     * @param        gnome
     */
    public void chefPart(Gnome gnome){
    }


    /**
     * @param        gnome
     */
    public void chefRevient(Gnome gnome){
        System.out.println("- " + gnome.nom + " [ISOLE] Le chef revient, je passe dans l'état protégé");
        gnome.changerEtat(new EtatProtege());
    }


    /**
     * @param        gnome
     */
    public void elfesReviennent(Gnome gnome){
        System.out.println("- " + gnome.nom + " [ISOLE] Les elfes reviennnent, je passe dans l'état vulnérable");
        gnome.changerEtat(new EtatVulnerable());
    }

    public void solliciter(Gnome gnome, Elfe elfe){
        System.out.println("- " + gnome.nom + " [ISOLE] Je suis solliciter par une autre tribu");
        gnome.deserter(elfe);
    }
}
