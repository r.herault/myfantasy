/**
 * Class Protege
 */
public class EtatProtege implements IEtat {

    //
    // Fields
    //


    //
    // Constructors
    //
    public EtatProtege () { };

    //
    // Methods
    //


    //
    // Accessor methods
    //

    //
    // Other methods
    //

    /**
     * @param        gnome
     */
    public void elfesPartent(Gnome gnome){
        System.out.println("- " + gnome.nom + " [PROTEGE] Les elfes partent, je passe dans l'état isolé");
        gnome.changerEtat(new EtatIsole());
    }


    /**
     * @param        gnome
     */
    public void chefPart(Gnome gnome){
        System.out.println("- " + gnome.nom + " [PROTEGE] Le chef part, je passe dans l'état vulnérable");
        gnome.changerEtat(new EtatVulnerable());
    }


    /**
     * @param        gnome
     */
    public void chefRevient(Gnome gnome){
    }


    /**
     * @param        gnome
     */
    public void elfesReviennent(Gnome gnome){
    }

    public void solliciter(Gnome gnome, Elfe elfe){
        
    }
}
