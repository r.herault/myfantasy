import java.util.Random;

/**
 * Class Vulnerable
 */
public class EtatVulnerable implements IEtat {

    //
    // Fields
    //


    //
    // Constructors
    //
    public EtatVulnerable () { };

    //
    // Methods
    //


    //
    // Accessor methods
    //

    //
    // Other methods
    //

    /**
     * @param        gnome
     */
    @Override
    public void elfesPartent(Gnome gnome){
        System.out.println("- " + gnome + " [VULNERABLE] Les elfes partent, je passe dans l'état isolé");
        gnome.changerEtat(new EtatIsole());
    }


    /**
     * @param        gnome
     */
    @Override
    public void chefPart(Gnome gnome){
    }


    /**
     * @param        gnome
     */
    @Override
    public void chefRevient(Gnome gnome){
        System.out.println("- " + gnome + " [VULNERABLE] Le chef revient, je passe dans l'état protégé");
        gnome.changerEtat(new EtatProtege());
    }


    /**
     * @param        gnome
     */
    @Override
    public void elfesReviennent(Gnome gnome){

    }

    public void solliciter(Gnome gnome, Elfe elfe){
        Random rand = new Random();
        int random = rand.nextInt(2) + 1; // Génération d'un nombre aléatoire entre 1 et 2
        if (random == 1){
            gnome.fuir();
        } else {
            gnome.deserter(elfe);
        }
    }
}
