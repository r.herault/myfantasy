import java.util.*;


/**
 * Class Gnome
 */
public class Gnome extends Personnage implements IEvenement {

    //
    // Fields
    //

    private IEtat etatCourant = new EtatProtege();
    private boolean solliciter;

    //
    // Constructors
    //
    public Gnome (String nom, IEtat etatCourant) {
        super(nom);
        this.etatCourant = etatCourant;
    }

    public Gnome(String nom){
        super(nom);
    }

    //
    // Methods
    //


    //
    // Accessor methods
    //

    /**
     * Get the value of etatCourant
     * @return the value of etatCourant
     */
    public IEtat getEtatCourant () {
        return this.etatCourant;
    }

    //
    // Other methods
    //

    /**
     */
    public void changerEtat(IEtat etat){
        this.etatCourant = etat;
    }

    public boolean getSolliciter(){
        return this.solliciter;
    }

    public void setSolliciter(boolean bool){
        this.solliciter = bool;
    }

    /**
     */
    public void refugier(){
        System.out.println("Je me déplace sur une autre parcelle en me rapprochant de mon chef");
    }


    /**
     */
    public void fuir(){
        System.out.println("- " + this.nom + " : Je me déplace sur une autre parcelle afin de fuir ce chef");
    }

    /**
     */
    public void deserter(Elfe elfe){
        System.out.println("- " + this.nom + " : Je change de tribu, ce chef m'a convaincu");
        this.getTribu().removePersonnage(this);
        this.setTribu(elfe.getTribu());
        this.etatCourant = new EtatProtege();
    }


    /**
     */
    public void elfesPartent(){
        etatCourant.elfesPartent(this);
    }


    /**
     */
    public void chefPart(){
        etatCourant.chefPart(this);
    }


    /**
     */
    public void elfesReviennent(){
        etatCourant.elfesReviennent(this);
    }


    /**
     */
    public void chefRevient(){
        etatCourant.chefRevient(this);
    }

    public void solliciter(Elfe elfe){
        etatCourant.solliciter(this, elfe);
    }

    public String toString(){
        return "\u001B[36m" + this.nom + "\u001B[0m";
    }
}
