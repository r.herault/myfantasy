
import java.util.*;


/**
 * Interface IEtat
 */
public interface IEtat {

    //
    // Fields
    //


    //
    // Methods
    //


    //
    // Accessor methods
    //

    //
    // Other methods
    //

    /**
     * @param        gnome
     */
    public void elfesPartent(Gnome gnome);


    /**
     * @param        gnome
     */
    public void chefPart(Gnome gnome);


    /**
     * @param        gnome
     */
    public void chefRevient(Gnome gnome);


    /**
     * @param        gnome
     */
    public void elfesReviennent(Gnome gnome);

    public void solliciter(Gnome gnome, Elfe elfe);
}
