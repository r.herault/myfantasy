
import java.util.*;


/**
 * Interface IStatut
 */
public interface IStatut {

    //
    // Fields
    //


    //
    // Methods
    //


    //
    // Accessor methods
    //

    //
    // Other methods
    //

    public void solliciter(Elfe elfe);

    public void emanciper();

    public void negocier();
}

