import java.util.Scanner;

public class Main{
    // Couleur dans le terminal
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static void main(String [] args) throws InterruptedException{
        // Initialisation du monde
        Parcelle parcelle = new Parcelle(10);

        Parcelle parcelleA = new Parcelle(10);

        Tribu tribuA = new Tribu();
        Elfe elfe1 = new Elfe("Tidur");
        elfe1.setTribu(tribuA);
        elfe1.setParcelleActuelle(parcelleA);
        elfe1.setStatut(new ElfeChef());
        tribuA.setElfeChef(elfe1);
        Gnome gnome1 = new Gnome("Ownyc");
        gnome1.setTribu(tribuA);
        gnome1.setParcelleActuelle(parcelleA);
        Gnome gnome2 = new Gnome("Yrer");
        gnome2.setTribu(tribuA);
        gnome2.setParcelleActuelle(parcelleA);

        Tribu tribuB = new Tribu();
        tribuB.addSousTribu(tribuA);
        Elfe elfe2 = new Elfe("Tinewenmir");
        elfe2.setTribu(tribuB);
        elfe2.setParcelleActuelle(parcelleA);
        elfe2.setStatut(new ElfeChef());
        tribuB.setElfeChef(elfe2);
        Elfe elfe3 = new Elfe("Iowenldor");
        elfe3.setTribu(tribuB);
        elfe3.setParcelleActuelle(parcelleA);
        Elfe elfe4 = new Elfe("Torfimbor");
        elfe4.setTribu(tribuB);
        elfe4.setParcelleActuelle(parcelleA);
        Gnome gnome3 = new Gnome("Ceryan", new EtatIsole()); // A CHANGER (ENLEVER L'ETAT ISOLE)
        gnome3.setTribu(tribuB);
        gnome3.setParcelleActuelle(parcelleA);
        Gnome gnome4 = new Gnome("Dynoic");
        gnome4.setTribu(tribuB);
        gnome4.setParcelleActuelle(parcelleA);
        Gnome gnome5 = new Gnome("Deanalyan", new EtatVulnerable()); // A CHANGER
        gnome5.setTribu(tribuB);
        gnome5.setParcelleActuelle(parcelleA);

        Parcelle parcelleB = new Parcelle(7);
        Tribu tribuC = new Tribu();
        Elfe elfe5 = new Elfe("Farith");
        elfe5.setTribu(tribuC);
        elfe5.setParcelleActuelle(parcelleB);
        elfe5.setStatut(new ElfeChef());
        tribuC.setElfeChef(elfe5);
        Elfe elfe6 = new Elfe("Unthien");
        elfe6.setTribu(tribuC);
        elfe6.setParcelleActuelle(parcelleB);
        Elfe elfe7 = new Elfe("Vydiarith");
        elfe7.setTribu(tribuC);
        elfe7.setParcelleActuelle(parcelleB);
        Gnome gnome6 = new Gnome("Ysnassa");
        gnome6.setTribu(tribuC);
        gnome6.setParcelleActuelle(parcelleB);
        Gnome gnome7 = new Gnome("Owullyn");
        gnome7.setTribu(tribuC);
        gnome7.setParcelleActuelle(parcelleB);
        Gnome gnome8 = new Gnome("Mullyn");
        gnome8.setTribu(tribuC);
        gnome8.setParcelleActuelle(parcelleB);
        Gnome gnome9 = new Gnome("Rhdeon");
        gnome9.setTribu(tribuC);
        gnome9.setParcelleActuelle(parcelleB);

        Parcelle parcelleC = new Parcelle(10);
        Tribu tribuD = new Tribu();
        tribuD.addSousTribu(tribuB);
        tribuD.addSousTribu(tribuC);
        Elfe elfe8 = new Elfe("Thrmir");
        elfe8.setTribu(tribuD);
        elfe8.setParcelleActuelle(parcelleC);
        elfe8.setStatut(new ElfeChef());
        tribuD.setElfeChef(elfe8);
        Elfe elfe9 = new Elfe("Folith");
        elfe9.setTribu(tribuD);
        elfe9.setParcelleActuelle(parcelleC);
        Elfe elfe10 = new Elfe("Gebrirn");
        elfe10.setTribu(tribuD);
        elfe10.setParcelleActuelle(parcelleC);
        Elfe elfe11 = new Elfe("Tebrindil");
        elfe11.setTribu(tribuD);
        elfe11.setParcelleActuelle(parcelleC);
        Elfe elfe12 = new Elfe("Glrionwen");
        elfe12.setTribu(tribuD);
        elfe12.setParcelleActuelle(parcelleC);
        Gnome gnome10 = new Gnome("Sonryllyn");
        gnome10.setTribu(tribuD);
        gnome10.setParcelleActuelle(parcelleC);
        Gnome gnome11 = new Gnome("Vallyn");
        gnome11.setTribu(tribuD);
        gnome11.setParcelleActuelle(parcelleC);
        Gnome gnome12 = new Gnome("Laevyanna");
        gnome12.setTribu(tribuD);
        gnome12.setParcelleActuelle(parcelleC);
        Gnome gnome13 = new Gnome("Brevyan");
        gnome13.setTribu(tribuD);
        gnome13.setParcelleActuelle(parcelleC);

        Parcelle parcelleD = new Parcelle(5);
        Tribu tribuE = new Tribu();
        Elfe elfe13 = new Elfe("Hebririth");
        elfe13.setTribu(tribuE);
        elfe13.setParcelleActuelle(parcelleD);
        elfe13.setStatut(new ElfeChef());
        tribuE.setElfeChef(elfe13);
        Elfe elfe14 = new Elfe("Cudiang");
        elfe14.setTribu(tribuE);
        elfe14.setParcelleActuelle(parcelleD);
        Gnome gnome14 = new Gnome("Brolla");
        gnome14.setTribu(tribuE);
        gnome14.setParcelleActuelle(parcelleD);
        Gnome gnome15 = new Gnome("Brenoic");
        gnome15.setTribu(tribuE);
        gnome15.setParcelleActuelle(parcelleD);

        Parcelle parcelleE = new Parcelle(10);
        Tribu tribuF = new Tribu();
        Elfe elfe15 = new Elfe("Legorion");
        elfe15.setTribu(tribuF);
        elfe15.setParcelleActuelle(parcelleE);
        elfe15.setStatut(new ElfeChef());
        Elfe elfe16 = new Elfe("Thrilmang");
        elfe16.setTribu(tribuF);
        elfe16.setParcelleActuelle(parcelleE);
        Elfe elfe17 = new Elfe("Eowiorion");
        elfe17.setTribu(tribuF);
        elfe17.setParcelleActuelle(parcelleE);
        Gnome gnome16 = new Gnome("Owdeor");
        gnome16.setTribu(tribuF);
        gnome16.setParcelleActuelle(parcelleE);
        Gnome gnome17 = new Gnome("Cardac");
        gnome17.setTribu(tribuF);
        gnome17.setParcelleActuelle(parcelleE);
        Gnome gnome18 = new Gnome("Addrer");
        gnome18.setTribu(tribuF);
        gnome18.setParcelleActuelle(parcelleE);
        Gnome gnome19 = new Gnome("Yroryn");
        gnome19.setTribu(tribuF);
        gnome19.setParcelleActuelle(parcelleE);

        // Parcelle voisine
        parcelle.addParcelleVoisine(parcelleA);
        parcelle.addParcelleVoisine(parcelleE);

        parcelleA.addParcelleVoisine(parcelle);
        parcelleA.addParcelleVoisine(parcelleC);

        parcelleB.addParcelleVoisine(parcelleA);
        parcelleB.addParcelleVoisine(parcelleD);

        parcelleC.addParcelleVoisine(parcelleA);
        parcelleC.addParcelleVoisine(parcelleE);

        parcelleD.addParcelleVoisine(parcelleB);
        parcelleD.addParcelleVoisine(parcelleE);

        parcelleE.addParcelleVoisine(parcelle);
        parcelleE.addParcelleVoisine(parcelleC);

        System.out.println(ANSI_BLUE + "Initialisation du monde..." + ANSI_RESET);Thread.sleep(2000);

        // Création du menu utilisateur
        Elfe joueur = new Elfe("Vous");
        Tribu tribu = new Tribu();
        joueur.setParcelleActuelle(parcelle);
        joueur.setStatut(new ElfeChef());
        joueur.setTribu(tribu);
        Gnome gnomeMate = new Gnome("Romain");
        gnomeMate.setParcelleActuelle(parcelle);
        gnomeMate.setTribu(tribu);
        System.out.println(ANSI_BLUE + "Initialisation du joueur..." + ANSI_RESET);Thread.sleep(1000);

        int rep = -1;
        Scanner sc = new Scanner(System.in);
        while(rep != 0){
            System.out.println(ANSI_YELLOW + "\n---------- Bienvenue dans MyFantasy ----------" + ANSI_RESET);
            System.out.println("0. Quitter");
            System.out.println("1. Gestion de l'univers");
            System.out.println("2. Mes informations");
            System.out.println("3. Se déplacer sur une parcelle voisine");
            System.out.println("4. Former sa tribu");
            if(joueur.getStatut() instanceof ElfeChef){
                System.out.println("5. Solliciter");
                System.out.println("6. S'émanciper");
                System.out.println("7. Negocier");
            }

            rep = sc.nextInt();
            sc.nextLine();

            switch(rep){
            case 0:
                System.out.println("Merci d'avoir joué !");
                System.exit(1);

            case 1:
                int rep2 = -1;
                Scanner sc2 = new Scanner(System.in);
                while(rep2 != 5){
                    System.out.println(ANSI_PURPLE + "\n---------- Gestion de l'univers ----------" + ANSI_RESET);
                    System.out.println("1. Ajouter un elfe");
                    System.out.println("2. Ajouter un gnome");
                    System.out.println("3. Ajouter une parcelle");
                    System.out.println("4. Ajouter une tribu");
                    System.out.println("5. Retour au menu principal");

                    rep2 = sc2.nextInt();
                    sc2.nextLine();

                    switch(rep2){
                    case 1:
                        System.out.println(ANSI_RED + "----- Ajout d'un elfe -----" + ANSI_RESET);
                        System.out.println("Nom de l'elfe : ");
                        String nom = sc2.nextLine();
                        Elfe elfe = new Elfe(nom);
                        elfe.setParcelleActuelle(parcelle);
                        elfe.setTribu(tribu);
                        System.out.println(ANSI_GREEN + "L'elfe a bien été ajouté !\n" + ANSI_RESET);break;

                    case 2:
                        System.out.println(ANSI_RED + "----- Ajout d'un gnome -----" + ANSI_RESET);
                        System.out.println("Nom du gnome : ");
                        String name = sc2.nextLine();
                        Gnome gnome = new Gnome(name);
                        gnome.setParcelleActuelle(parcelle);
                        gnome.setTribu(tribu);
                        System.out.println(ANSI_GREEN + "Le gnome a bien été ajouté !\n" + ANSI_RESET);break;

                    case 3:
                        System.out.println(ANSI_RED + "----- Ajout d'une parcelle -----" + ANSI_RESET);
                        System.out.println("Nombre de place dans la parcelle : ");
                        int place = sc2.nextInt();
                        //Parcelle parcelle = new Parcelle(place);
                        System.out.println(ANSI_GREEN + "La parcelle a bien été ajoutée ! (en réalité ça ne fonctionne pas. TODO)\n" + ANSI_RESET);break;
                    case 4:
                        System.out.println(ANSI_RED + "----- Ajout d'une tribu -----" + ANSI_RESET);
                        //Tribu tribu = new Tribu();
                        System.out.println(ANSI_GREEN + "La tribu a bien été ajoutée !(en réalité ça ne fonctionne pas. TODO)\n" + ANSI_RESET);break;
                    case 5:
                        break;
                    default:
                        System.out.println("Commande inconnue 😥");
                    }
                }
                break;
            case 2:
                int rep3 = -1;
                Scanner sc3 = new Scanner(System.in);
                while(rep3 != 5){
                    System.out.println(ANSI_PURPLE + "\n---------- Mes informations ----------" + ANSI_RESET);
                    System.out.println("1. Afficher ma parcelle");
                    System.out.println("2. Afficher ma tribu");
                    System.out.println("5. Retour au menu principal");

                    rep3 = sc3.nextInt();
                    sc3.nextLine();

                    switch(rep3){
                    case 1:
                        System.out.println(ANSI_RED + "----- Affichage de ma parcelle -----" + ANSI_RESET);
                        System.out.println(joueur.getParcelleActuelle() + "\n");break;
                    case 2:
                        System.out.println(ANSI_RED + "----- Affichage de ma tribu -----" + ANSI_RESET);
                        System.out.println(joueur.getTribu() + "\n");break;
                    case 5:
                        break;
                    default:
                        System.out.println("Commande inconnue 😥");
                    }
                }
                break;
            case 3:
                System.out.println(ANSI_RED + "----- Déplacement -----" + ANSI_RESET);
                for(int i = 1; i <  joueur.getParcelleActuelle().getListeParcelleVoisine().size() + 1; i++){
                    System.out.println(i + ". " + joueur.getParcelleActuelle().getListeParcelleVoisine().get(i - 1));
                }
                System.out.println("Numéro de la parcelle dans laquelle se déplacer : ");
                int num = sc.nextInt();
                joueur.seDeplacer((Parcelle) joueur.getParcelleActuelle().getListeParcelleVoisine().get(num-1));
                System.out.println(ANSI_GREEN + "Vous vous êtes bien déplacé sur cette parcelle ! " + ANSI_RESET);break;

            case 4:
                System.out.println(ANSI_YELLOW + "WORK IN PROGRESS.." + ANSI_RESET);
                break;

            case 5:
                System.out.println(ANSI_RED + "----- Sollicitation -----" + ANSI_RESET);
                joueur.solliciter(joueur);
                break;

            case 6:
                System.out.println(ANSI_YELLOW + "WORK IN PROGRESS.." + ANSI_RESET);
                break;

            case 7:
                System.out.println(ANSI_YELLOW + "WORK IN PROGRESS.." + ANSI_RESET);
                break;

            default:
                System.out.println("Commande inconnue 😥");
            }
        }
    }
}
