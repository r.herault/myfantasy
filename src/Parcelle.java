import java.util.*;


/**
 * Class Parcelle
 */
public class Parcelle {

    //
    // Fields
    //


    private ArrayList<Personnage> listePersonnageParcelle = new ArrayList();
    private ArrayList<Parcelle> listeParcelleVoisine = new ArrayList();
    private int nbPlaces;

    //
    // Constructors
    //
    public Parcelle (int nbPlaces) {
        this.nbPlaces = nbPlaces;
    }

    //
    // Methods
    //


    //
    // Accessor methods
    //

    /**
     * Add a ListePersonnageParcelle object to the listePersonnageParcelle List
     */
    public boolean addPersonnageParcelle (Personnage pers) {
        if(!(this.listePersonnageParcelle.size() > this.nbPlaces - 1)){
            listePersonnageParcelle.add(pers);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Remove a ListePersonnageParcelle object from listePersonnageParcelle List
     */
    public void removePersonnageParcelle (Personnage pers)
    {
        listePersonnageParcelle.remove(pers);
    }

    public void addParcelleVoisine(Parcelle parcelle){
        this.listeParcelleVoisine.add(parcelle);
    }

    public void removeParcelleVoisine(Parcelle parcelle){
        this.listeParcelleVoisine.remove(parcelle);
    }


    public int getNbPlaces(){
        return this.nbPlaces;
    }

    public void setNbPlaces(int nbPlaces){
        this.nbPlaces = nbPlaces;
    }

    /**
     * Get the List of ListePersonnageParcelle objects held by
     * listePersonnageParcelle
     * @return List of ListePersonnageParcelle objects held by
     * listePersonnageParcelle
     */
    public List<Personnage> getListePersonnageParcelle() {
        return (List<Personnage>) listePersonnageParcelle;
    }

    public List getListeParcelleVoisine(){
        return (List) listeParcelleVoisine;
    }


    //
    // Other methods
    //
    public String toString(){
        String res = "Personnage de la parcelle :";
        for(Personnage perso : this.listePersonnageParcelle){
            res += " " + perso;
        }
        return res;
    }
}
