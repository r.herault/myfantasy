import java.util.*;


/**
 * Class Personnage
 */
public abstract class Personnage {

    //
    // Fields
    //

    private Parcelle parcelleActuelle;

    private Tribu tribu;

    protected String nom;

    //
    // Constructors
    //
    public Personnage (String nom) {
        this.nom = nom;
    }

    //
    // Methods
    //


    //
    // Accessor methods
    //

    /**
     * Set the value of parcelleActuelle
     * @param parcelle the new value of parcelleActuelle
     */
    public void setParcelleActuelle (Parcelle parcelle) {
        if(parcelle.addPersonnageParcelle(this)){
            this.parcelleActuelle = parcelle;
        } else {
            System.out.println("La parcelle est pleine !");
        }
    }

    public void changeParcelleActuelle(Parcelle parcelle){
        if(parcelle.addPersonnageParcelle(this)){
            this.parcelleActuelle.removePersonnageParcelle(this);
            this.parcelleActuelle = parcelle;
        }else{
            System.out.println("La parcelle est pleine !");
        }
    }

    /**
     * Get the value of parcelleActuelle
     * @return the value of parcelleActuelle
     */
    public Parcelle getParcelleActuelle () {
        return this.parcelleActuelle;
    }

    /**
     * Set the value of tribu
     * @param tribu the new value of tribu
     */
    public void setTribu (Tribu tribu) {
        this.tribu = tribu;
        tribu.addPersonnage(this);
    }

    /**
     * Get the value of tribu
     * @return the value of tribu
     */
    public Tribu getTribu () {
        return this.tribu;
    }

    //
    // Other methods
    //

}
