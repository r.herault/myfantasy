import java.util.*;


/**
 * Class Tribu
 */
public class Tribu {

    //
    // Fields
    //


    private ArrayList<Personnage> listePersonnage = new ArrayList();

    private ArrayList<Tribu> sousTribu = new ArrayList();

    protected Elfe elfeChef;
    //
    // Constructors
    //
    public Tribu () { };

    //
    // Methods
    //


    //
    // Accessor methods
    //

    /**
     * Add a ListePersonnage object to the listePersonnage List
     */
    public void addPersonnage (Personnage personnage){
        listePersonnage.add(personnage);
    }

    /**
     * Remove a ListePersonnage object from listePersonnage List
     */
    public void removePersonnage (Personnage personnage){
        listePersonnage.remove(personnage);
    }

    /**
     * Get the List of ListePersonnage objects held by listePersonnage
     * @return List of ListePersonnage objects held by listePersonnage
     */
    public List getListePersonnage() {
        return (List) listePersonnage;
    }

    public void addSousTribu(Tribu tribu){
        this.sousTribu.add(tribu);
    }

    public void removeSousTribu(Tribu tribu){
        this.sousTribu.remove(tribu);
    }

    public List getSousTribu(){
        return this.sousTribu;
    }

    public void setElfeChef(Elfe elfe){
        this.elfeChef = elfe;
    }

    public Elfe getElfeChef(){
        return this.elfeChef;
    }

    //
    // Other methods
    //
    public String toString(){
        String res = "Personnage de la tribu :";
        for(Personnage perso : this.listePersonnage){
            res += " " + perso;
        }
        res += "\nSous tribu de cette tribu :";
        for(Tribu tribu : this.sousTribu){
            res += " " + tribu;
        }
        return res;
    }
}
